var screenW=document.getElementsByTagName('body')[0].clientWidth;

blue.pos = [0, 0];
red.pos = [40, 0];
green.pos = [60, 0];
yellow.pos = [100, 0];

let speed = 100;
//actions: hop, wait, peck

function doSomething(bird, queue) {
  if (!queue.length) { // if queue is empty
    // console.log('wait, choose action')
    setTimeout(() => {
      chooseAction(bird) // choose action
    }, speed*5) // wait 5 seconds
  } else {
    var action = queue.shift();
    // console.log(action)
    if (action === 'hop') {
      var i=0; //frame count
      var d=1; //1=right, 0=left
      if (bird.pos[0] >= screenW-32) { //offscreen check
        d = 0; // move left
      } else if (bird.pos[0] <= 0) {
        d = 1; // move right
      } else {
        d = Math.floor(Math.random()*2); // move random
      }

      setTimeout(() => {
        bird.style.transform = d ? "none" : "scaleX(-1)"
        hop(bird, i, d)
        doSomething(bird, queue) //trigger loop repeating at interval of var speed
      }, speed*1.2)
    } else if (action === 'peck') {
      var i=0; //frame count
      var t = Math.floor(Math.random()*2) //peck count
      setTimeout(() => {
        peck(bird, i, t)
        doSomething(bird, queue)
      }, speed*1.5)
    }
  }
}

function hop(bird, i, d) {
  if (i<10) {
    setTimeout(() => {
      if ([0,2,4,5,7,9].includes(i)) {
        bird.style.left = (bird.pos[0]+=4*(d ? 1 : -1))+'px'; //lord
      } else if ([1, 3].includes(i)) {
        bird.style.bottom = (bird.pos[1]+=4)+'px';
      } else {
        bird.style.bottom = (bird.pos[1]-=4)+'px';
      }
      i++;
      hop(bird, i, d)
    }, speed*.1)
  }
}

function peck(bird, i, t) {
  if (i<15) {
    setTimeout(() => {
      if ([0,1,2,3,4,5,6,7,8].includes(i)) {
        bird.src = `./${bird.id[0]}2.png`;
      } else {
        bird.src = `./${bird.id[0]}1.png`;
      }
      i++;
      peck(bird, i, t)
    }, speed*.1)
  }
}

function chooseAction(bird) {
  //get the bird moving
  let actions = ['hop', 'peck'];
  var roll = Math.floor(Math.random()*2);
  var times = 0;
  var queue = [];

  if (roll) { // roll, if 0, hop up to 5 times
    times = 5;
  } else { // if 1, peck up to twice
    times = 2;
  }

  for(var i=0; i<Math.floor(Math.random()*times); i++) {
    queue.push(actions[roll])
  }

  doSomething(bird, queue) // execute queue
}



doSomething(blue, []) // start moving bird
doSomething(red, [])
doSomething(yellow, [])
doSomething(green, [])

// _____ CLOUDS _____________
var cloud = document.querySelectorAll('.cloud')

function wind(i) { //add layers to each cloud
  if (i < cloud.length) {
    setTimeout(() => {
      cloud[i].classList.add('cloud-layer-'+(Math.floor(Math.random()*4)+1));
      cloud[i].style.top = Math.floor(Math.random()*55)+"vh";
      cloud[i].style.webkitAnimationPlayState = 'running';
      i++;
      wind(i);
    }, Math.floor(Math.random()*5000))
  }
}
wind(0) //start clouds

function spawnEnemy(enemy) {
  let body = document.querySelector('#enemy');
  console.log(enemy)

  switch(enemy) {
    case 'lakitu':
      body.src = "lakitu1.png";
      body.classList.add('lakitu');
      body.style.top = Math.floor(Math.random()*55)+"vh";
      body.style.webkitAnimationPlayState = 'running';
      setTimeout(() => {
        //finish
      }, 3000)
      break;
  }
}

spawnEnemy('lakitu')
